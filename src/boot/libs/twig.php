<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 15.08.16
 * Time: 10:13
 */
\Larakit\Twig::register_function('class_current_url', function ($url, $class) {
    $current_url = trim(\URL::current(), '/');
    $url         = trim($url, '/');

    return str_is($url . '*', $current_url) ? ' ' . $class : '';
});
\Larakit\Twig::register_function('class_current_route', function ($route, $class) {
    $current_route = Route::currentRouteName();

    return str_is($route . '*', $current_route) ? ' ' . $class : '';
});
