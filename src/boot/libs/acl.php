<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 08.08.16
 * Time: 9:10
 */
 
 \Larakit\Twig::register_function('accessor', function($model){
     return \Larakit\Accessors\Accessor::factory($model);
 });