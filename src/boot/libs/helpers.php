<?php
//################################################################################
// менеджер помощников
//################################################################################
\Larakit\Managers\ManagerHelper::register('text', \Larakit\Helpers\HelperText::class);
\Larakit\Managers\ManagerHelper::register('phone', \Larakit\Helpers\HelperPhone::class);
\Larakit\Managers\ManagerHelper::register('date', \Larakit\Helpers\HelperDate::class);
\Larakit\Managers\ManagerHelper::register('file', \Larakit\Helpers\HelperFile::class);
\Larakit\Managers\ManagerHelper::register('model', \Larakit\Helpers\HelperModel::class);

