<?php
$composers_file = app_path('Http/composers.php');
if(file_exists($composers_file)) {
    require_once $composers_file;
}
$composers_file = base_path('bootstrap/composers.php');
if(file_exists($composers_file)) {
    require_once $composers_file;
}
