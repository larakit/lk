<?php
return [
    'months'         => [
        1 => 'იანვარს',
        2 => 'თებერვალს',
        3 => 'მარტს',
        4 => 'აპრილს',
        5 => 'მაისს',
        6 => 'ივნისს',
        7 => 'ივლისს',
        8 => 'აგვისტოს',
        9 => 'სექტემბერს',
        10 => 'ოქტომბერს',
        11 => 'ნოემბერს',
        12 => 'დეკემბერს',
    ],
    'months_i'       => [
        1 => 'იანვარს',
        2 => 'თებერვალს',
        3 => 'მარტს',
        4 => 'აპრილს',
        5 => 'მაისს',
        6 => 'ივნისს',
        7 => 'ივლისს',
        8 => 'აგვისტოს',
        9 => 'სექტემბერს',
        10 => 'ოქტომბერს',
        11 => 'ნოემბერს',
        12 => 'დეკემბერს',
    ],
    'weekdays'       => [
        1 => 'понедельник',
        2 => 'вторник',
        3 => 'среда',
        4 => 'четверг',
        5 => 'пятница',
        6 => 'суббота',
        7 => 'воскресенье',
    ],
    'weekdays_short' => [
        1 => 'ПН',
        2 => 'ВТ',
        3 => 'СР',
        4 => 'ЧТ',
        5 => 'ПТ',
        6 => 'СБ',
        7 => 'ВС',
    ],
];