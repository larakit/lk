<?php
//регистрируем провайдеры
Larakit\Boot::register_provider(Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
Larakit\Boot::register_command(\Larakit\Commands\AppDb::class);
Larakit\Boot::register_command(\Larakit\Commands\AppMiddlewares::class);
Larakit\Boot::register_boot(__DIR__.'/boot');
Larakit\Boot::register_view_path(__DIR__.'/views', 'larakit');
Larakit\Boot::register_lang(__DIR__.'/lang', 'larakit');


//\Larakit\StaticFiles\Manager::package('larakit/larakit-filemanager')
//    ->setSourceDir('larakit-filemanager');
//
//\Larakit\StaticFiles\Manager::package('larakit/larakit-wysiwyg')
//    ->setSourceDir('larakit-wysiwyg')
//    ->jsPackage('ckeditor/ckeditor.js')
//    ->cssPackage('ckeditor/contents.css')
//    ->jsPackage('laraform-wysiwyg.js');