<?php
namespace Larakit\Managers;

class ManagerHelper {

    /**
     * Регистрация хелпера
     *
     * @param      $package
     * @param bool $dir
     */
    static function register($helper, $class) {
        \Larakit\Twig::register_function('helper_' . $helper, function ($method) use ($class) {
            if(!class_exists($class)) {
                return null;
            }
            $args = array_slice(func_get_args(), 1);

            return call_user_func_array([
                $class,
                $method,
            ],
                $args);
        });

    }
}