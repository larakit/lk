<?php
namespace Larakit\Commands;

use Illuminate\Console\Command;
use Larakit\Boot;

class AppMiddlewares extends Command {
    
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'larakit:show:middlewares';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Показать все зарегистрированные middlewares';
    
    /**
     * Пусть для сохранения сгенерированных данных
     *
     * @var string
     */
    protected $path;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        $this->error(str_repeat('#', 70));
        $this->error('#' . str_pad('MIDDLEWARES', 68, ' ', STR_PAD_BOTH) . '#');
        $this->error(str_repeat('#', 70));
        $rows = [];
        foreach(Boot::middlewares() as $m) {
            $rows[] = [
                $m,
            ];
        }
        $this->table(['Middleware'], $rows);
        
        $this->error(str_repeat('#', 70));
        $this->error('#' . str_pad('MIDDLEWARES GROUP', 68, ' ', STR_PAD_BOTH) . '#');
        $this->error(str_repeat('#', 70));
        $rows = [];
        foreach(Boot::middlewares_group() as $group => $ms) {
            $rows[] = [
                $group,
            ];
            foreach($ms as $m) {
                $rows[] = [
                    '',
                    $m,
                ];
            }
        }
        $this->table(['Group', 'Middleware'], $rows);
    
        $this->error(str_repeat('#', 70));
        $this->error('#' . str_pad('MIDDLEWARES ROUTE', 68, ' ', STR_PAD_BOTH) . '#');
        $this->error(str_repeat('#', 70));
        $rows = [];
        foreach(Boot::middlewares_route() as $name => $m) {
            $rows[] = [
                $name,
                $m,
            ];
        }
        $this->table(['Name', 'Middleware'], $rows);
    }
    
    public function getArguments() {
        return [];
    }
    
}