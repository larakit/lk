<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 05.08.16
 * Time: 14:08
 */

namespace Larakit\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Larakit\CRUD\TraitEntity;
use Larakit\Event\Event;

/**
 * Class Model
 *
 * @package Larakit\Models
 * @mixin \Eloquent
 * @mixin TraitEntity
 * @property-read mixed $js_sort
 * @property-read mixed $js_filter
 * @property-read mixed $to_string
 * @method static \Illuminate\Database\Query\Builder|\Larakit\Models\LarakitModel listsExt($key = 'id')
 * @method static \Illuminate\Database\Query\Builder|\Larakit\Models\LarakitModel sorted()
 */
class LarakitModel extends \Illuminate\Database\Eloquent\Model {

    const EVENT_JS_FILTER_ATTRIBUTE = 'larakit::model::getJsFilterAttribute';

    use TraitEntity;

    protected $appends      = [
        'js_filter',
        'js_sort',
        'toString',
    ];
    protected $default_sort = [];

    static function getGroupColumnValue() {
        return null;
    }

//    function toArray() {
//        $ret = parent::toArray();
//        foreach($ret as $key => $value) {
//            switch(true) {
//                case is_float($value):
//                    $ret[$key] = (float) $value;
//                    break;
//                case is_numeric($value):
//                    $ret[$key] = (int) $value;
//                    break;
//                default:
//                    break;
//            }
//        }
//
//        return $ret;
//    }

    static function isUseThumb() {
        return method_exists(get_called_class(), 'thumb');
    }

    static function isUseBelongsToMany() {
        return false;
    }

    static function isUseHasMany() {
        return false;
    }

    static function isUseThumbs() {
        return method_exists(get_called_class(), 'larakitImages');
    }

    static function isUseAttaches() {
        return method_exists(get_called_class(), 'larakitAttaches');
    }

    static function getModelKey() {
        return md5(static::class);
    }
    protected $sort_len = 9;

    /**
     * Поле для сортировки для LarakitManager
     * @return int
     */
    function getJsSortAttribute() {
        $sort = [];
        if(!$this->default_sort) {
            return str_pad($this->id, 10, '0', STR_PAD_LEFT);
        }
        foreach($this->default_sort as $k => $v) {
            $val = $this->getAttribute($k);
            if('desc' == $v) {
                if($val instanceof Carbon) {
                    //$val = $val->format('Y-m-d-H-i-s');
                    $val = $val->timestamp;
                    $len = mb_strlen($val);
                }

                if(is_numeric($val)) {
                    $val = pow(10, $this->sort_len) - intval($val);
                }
            }
            $sort[] = str_pad(mb_substr($val, 0, $this->sort_len + 1), $this->sort_len + 1, '0', STR_PAD_LEFT);
        }

        return implode('__', $sort);
    }

    function getJsFilterAttribute() {
        $group     = static::getGroupColumn();
        $default   = [];
        $default[] = $this->id;
        if($group) {
            $default[] = $group . '_' . $this->{$group};
        }

        return Event::filter(self::EVENT_JS_FILTER_ATTRIBUTE, $default, [], $this);
    }

    static function getGroupColumn() {
        return null;
    }

    function scopeListsExt(Builder $query, $key = 'id') {
        $ret = [];
        foreach($query->get() as $model) {
            $ret[$model->{$key}] = $model->__toString();
        }

        return $ret;
    }

    protected $form_appends = [];

    function toForm() {
        $ret = $this->toArray();
        foreach($this->getDates() as $dt) {
            if($this->{$dt}) {
                $ret[$dt] = Carbon::parse($this->{$dt})
                                  ->format('d.m.Y H:i:s');
            }
        }
        foreach($this->form_appends as $f) {
            $ret[$f] = $this->{$f};
        }

        return $ret;
    }

    function getToStringAttribute() {
        return $this->__toString();
    }

    function scopeSorted(Builder $query) {
        foreach($this->default_sort as $k => $v) {
            $query->orderBy($k, $v);
        }
    }
}