<?php
namespace Larakit\Accessors;

use Illuminate\Support\Arr;
use Larakit\ACL\Acl;
use Larakit\CRUD\TraitEntity;
use Larakit\Helpers\HelperDate;
use Larakit\Helpers\HelperModel;
use Larakit\Html\Toggler;
use Larakit\Models\LarakitModel;
use Laravelrus\LocalizedCarbon\LocalizedCarbon;

class Accessor {

    use TraitEntity;

    protected $model;

    /**
     * @var Model
     */

    function __construct($model) {
        $this->model = $model;
    }

    static function getEntityPrefix() {
        return 'accessor';
    }

    function getSwitchMode($attribute) {
        return Arr::get($this->switchers, $attribute, false);
    }

    function __get($name) {
        return $this->get($name);
    }

    function get() {
        try {
            $args = func_get_args();
            $name = array_shift($args);
            if(!$name) {
                return (string) \HtmlSpan::addClass('label label-danger')->setContent('Accessor:get()');
            }
            $field = $this->model->{$name};
            if(isset($field) && is_object($field) && $field instanceof Model) {
                return Accessor::factory($field);
            }
            $method = 'get' . studly_case($name) . 'Attribute';
            if('getAttribute' == $method) {
                return null;
            }
            if(method_exists($this, $method)) {
                return call_user_func_array([
                    $this,
                    $method,
                ], $args);
            }

            return $this->model->{$name};
        }
        catch(\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $model
     *
     * @return Accessor
     */
    static function factory($model) {
        /** @var LarakitModel $model_class */
        $model_class    = get_class($model);
        $accessor_class = $model_class::classAccessor();
        if(!class_exists($accessor_class)) {
            $accessor_class = __CLASS__;
        }

        return new $accessor_class($model);

    }

    function model() {
        return $this->model;
    }

    function __toString() {
        try {
            return (string) $this->model->__toString();
        }
        catch(\Exception $e) {
            dd($e);

            return "";
        }
    }

    function actionEdit($type = null) {
        if(!Acl::factory($this->model)
            ->reason('edit')
        ) {
            $ret = \HtmlButton::setAttribute('data-action', 'edit')
                ->addClass('js-crud-edit btn btn-box-tool')
                ->setTitle('Редактирование')
                ->setContent('<i class="fa fa-pencil"></i>');
            if($type) {
                $ret->setAttribute('data-type', $type);
            }

            return $ret;
        }

        return '';
    }

//    function actionBelongsTo($relation, $icon_class = 'fa fa-sitemap') {
//        if(!Acl::factory($this->model)
//            ->reason('edit')
//        ) {
//            $btn = \HtmlButton::setAttribute('data-action', 'belongs_to')
//                ->setAttribute('data-relation', $relation)
//                ->addClass('btn btn-primary btn-xs js-btn mr10')
//                ->setTitle('Редактировать связь')
//                ->setContent('<i class="' . $icon_class . '"></i>');;
//            $rel = $this->model->{$relation};
//            /* TODO: сделать получение "нет привязки к группе" через laralang модели */
//            if(!$rel) {
//                $rel = \HtmlSpan::addClass('text-muted')
//                    ->setContent('- не выбрано -');
//            }
//
//            return \HtmlDiv::addClass('row-fluid')
//                ->setContent(\HtmlDiv::addClass('pull-left')
//                        ->setContent($btn) . \HtmlDiv::setContent($rel));
//        }
//
//        return '';
//    }

    function actionThumb() {
        if(!Acl::factory($this->model)->reason('thumb')) {
            return \HtmlButton::addClass('js-crud-thumb btn btn-box-tool')
                ->setTitle('Управление иллюстрациями модели')
                ->setContent('<i class="fa fa-image"></i>');
        }

        return '';
    }

    function actionRelHasMany() {
        if(!Acl::factory($this->model)->reason('rel_has_many')) {
            return \HtmlButton::addClass('js-crud-rel-has-many btn btn-box-tool')
                ->setTitle('Управление связями типа "один ко многим"')
                ->setContent('<i class="fa fa-th-list"></i>');
        }

        return '';
    }

    function actionRelBelongsToMany() {
        if(!Acl::factory($this->model)->reason('rel_belongs_to_many')) {
            return \HtmlButton::addClass('js-crud-rel-belongs-to-many btn btn-box-tool')
                ->setTitle('Управление связями типа "многие ко многим"')
                ->setContent('<i class="fa fa-sitemap"></i>');
        }

        return '';
    }

    function actionThumbs() {
        if(!Acl::factory($this->model)->reason('thumbs')) {
            return \HtmlButton::addClass('js-crud-thumbs btn btn-box-tool')
                ->setTitle('Управление галереями')
                ->setContent('<i class="fa fa-clone"></i>');
        }

        return '';
    }

    function actionAttaches() {
        if(!Acl::factory($this->model)->reason('attaches')) {
            return \HtmlButton::addClass('js-crud-attaches btn btn-box-tool')
                ->setTitle('Управление вложениями')
                ->setContent('<i class="fa fa-paperclip"></i>');
        }

        return '';
    }

    function actionDelete() {
        if(!Acl::factory($this->model)
            ->reason('delete')
        ) {
            return \HtmlButton::setAttribute('data-action', 'delete')
                ->addClass('btn btn-danger btn-xs js-btn')
                ->setTitle('Удалить запись')
                ->setContent('<i class="fa fa-trash"></i>');
        }

        return '';
    }

    function toggleInverse($field, $field_extend = []) {
        return $this->toggle($field, $field_extend)
            ->setOffClass('btn-success')
            ->setOnClass('btn-danger');
    }

    /**
     * @param       $field
     * @param array $field_extend
     *
     * @return Toggler
     */
    function toggle($field, $field_extend = []) {
        $toggler = new Toggler();
        $toggler->setState($this->model->{$field})
            ->setLabel($this->model->getEntityLabel($field))
            ->setAttribute('data-action', 'toggle')
            ->setAttribute('data-field', $field)
            ->setAttribute('data-field-extend', http_build_query($field_extend));

        return $toggler;
    }

    /**
     * @param       $field
     * @param array $field_extend
     *
     * @return Toggler
     */
    function toggleFrozen($field, $field_extend = []) {
        return $this->toggle($field, $field_extend)
            ->removeClass('pointer')
            ->setLabel('');

    }

    function rows($ext = []) {
        $ret         = [];
        $model_class = get_class($this->model);
        if($model_class::isUseThumb()) {
            foreach($model_class::thumbTypes() as $type) {
                $ret['thumb_' . $type] = $this->row('thumb', [
                    'thumb_type' => $type,
                ]);
            }
        }

        foreach($this->rowTypes() as $row_type) {
            $ret[$row_type] = $this->row($row_type, $ext);
        }

        return $ret;
    }

    function row($type = null, $ext = []) {
        try {
            $model_class = get_class($this->model);

            $tpl         = $model_class::getVendorSnake() . '!.crud.rows.' . $model_class::getEntitySnake() . '.' . $type;
            if(!\View::exists($tpl)) {
                $tpl = 'larakit::!.crud.rows._.' . $type;
            }
            return (string) \View::make($tpl, array_merge((array) $ext, [
                'model' => $this->model,
            ]));
        }
        catch(\Exception $e) {
            print '<pre>';
            echo $e->getMessage();
            echo $e->getTraceAsString();
            print '</pre>';
            exit;
        }
    }

    function rowTypes() {
        return ['_'];
    }

    function publicLink() {
        return \HtmlA::setHref($this->publicUrl())
            ->setContent($this->model);
    }

    function publicUrl() {
        return '/' . $this->model->id . '/';
    }

    function card($fields) {
        return \View::make('larakit::!.partials..card', [
            'fields' => $fields,
            'model'  => $this->model,
        ]);
    }

    function widgetHasMany($relations) {
        $model_name = get_class($this->model);
        if(!$relations) {
            $relations = array_keys(HelperModel::getHasMany($model_name));
        }

        return WidgetHasMany::factory($model_name . $this->model->id)
            ->setModel($this->model)
            ->setRelations($relations) . '';
    }

    function toArray() {
        $ret = [];
        foreach($this->model->toArray() as $k => $v) {
            $ret[$k] = $this->get($k);
        }

        return $ret;
    }

    function itemToggler() {
        return \HtmlButton::addClass('btn btn-default btn-xs js-collapse-item mr10 t-3')
            ->setAttribute('data-widget', 'collapse')
            ->setAttribute('type', 'button')
            ->setContent('<i class="fa fa-plus"></i>');
    }

    function itemDrag() {
        return \HtmlSpan::addClass('btn btn-default btn-xs sortable-handle mr10 t-3')
            ->setContent('<i class="fa fa-bars"></i>');
    }

    function isDeletedClass() {
        return (isset($this->model->deleted_at) && $this->model->deleted_at) ? 'deleted_record' : '';
    }

    function getUpdatedAtAttribute() {
        return \HtmlAbbr::setContent($this->getUpdatedAtDiffAttribute())
            ->setTitle($this->getUpdatedAtDateAttribute());
    }

    function getUpdatedAtDiffAttribute() {
        return LocalizedCarbon::parse($this->model->updated_at)
            ->diffForHumans();
    }

    function getUpdatedAtDateAttribute() {
        return HelperDate::fromDatetime($this->model->updated_at);
    }

    function getCreatedAtAttribute() {
        return \HtmlAbbr::setContent($this->getCreatedAtDiffAttribute())
            ->setTitle($this->getCreatedAtDateAttribute());
    }

    function getCreatedAtDiffAttribute() {
        return LocalizedCarbon::parse($this->model->created_at)
            ->diffForHumans();
    }

    function getCreatedAtDateAttribute() {
        return HelperDate::fromDatetime($this->model->created_at);
    }

    function getRowClassAttribute() {
        return 'primary';
    }

}