<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 07.08.16
 * Time: 19:56
 */

namespace Larakit;

class SEO {

    static $data = [];

    static function title($route, $val = null) {
        static::$data[__FUNCTION__][$route] = $val;
    }

    static function h1($route, $val = null) {
        static::$data[__FUNCTION__][$route] = $val;
    }

    static function description($route, $val = null) {
        static::$data[__FUNCTION__][$route] = $val;
    }

    static function h1_ext($route, $val = null) {
        static::$data[__FUNCTION__][$route] = $val;
    }

}