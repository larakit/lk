<?php
namespace Larakit\Helpers;

use Illuminate\Support\Arr;
use Sami\Parser\DocBlockParser;
use Sami\Parser\Filter\TrueFilter;
use Sami\Parser\ParserContext;

class HelperModel {

    static function getBelongsTo($class_model) {
        $o       = new $class_model;
        $methods = self::getMethods($o);
        $ret     = [];
        foreach($methods as $method_name => $method) {
            $source = Arr::get($method, 'source');
            if('belongsTo' != $method_name) {
                if(false !== mb_strpos($source, 'belongsTo(')) {
                    if('__call' != $method_name) {
                        $ret[$method_name] = get_class($o->{$method_name}()
                            ->getRelated());
                    }
                }
            }
        }

        return $ret;
    }

    static function getMethods($model) {
        $ret  = [];
        $refl = new \ReflectionClass($model);
        foreach($refl->getMethods() as $method) {
            $dbp                                  = new DocBlockParser();
            $filter                               = new TrueFilter();
            $prettyPrinter                        = new \PhpParser\PrettyPrinter\Standard();
            $context                              = new ParserContext($filter, $dbp, $prettyPrinter);
            $doc                                  = $dbp->parse($method->getDocComment(), $context);
            $ret[$method->getName()]['shortDesc'] = $doc->getShortDesc();
            $ret[$method->getName()]['longDesc']  = $doc->getLongDesc();
            $ret[$method->getName()]['tags']      = $doc->getTags();

            $ret[$method->getName()]['method'] = $method;
            $relf_text                         = file($method->getFileName());

            $source = Arr::only($relf_text, range($method->getStartLine() - 1, $method->getEndLine() - 1));

            $source                            = array_map('trim', $source);
            $ret[$method->getName()]['source'] = implode('', $source);
            $ret[$method->getName()]['start']  = $method->getStartLine();
            $ret[$method->getName()]['end']    = $method->getEndLine();
        }

        return $ret;
    }

    static function getMethod($model, $method_name) {
        $method = new \ReflectionMethod($model, $method_name);

        $dbp              = new DocBlockParser();
        $filter           = new TrueFilter();
        $prettyPrinter    = new \PhpParser\PrettyPrinter\Standard();
        $context          = new ParserContext($filter, $dbp, $prettyPrinter);
        $doc              = $dbp->parse($method->getDocComment(), $context);
        $ret['shortDesc'] = $doc->getShortDesc();
        $ret['longDesc']  = $doc->getLongDesc();
        $ret['tags']      = $doc->getTags();

        $ret['method'] = $method;
        $relf_text     = file($method->getFileName());

        $source = Arr::only($relf_text, range($method->getStartLine() - 1, $method->getEndLine() - 1));

        $source        = array_map('trim', $source);
        $ret['source'] = $source = implode('', $source);
        $ret['start']  = $method->getStartLine();
        $ret['end']    = $method->getEndLine();

        switch(true){
            case mb_strpos($source, 'belongsToMany');
                $rel_type = 'belongsToMany';
                break;
            case mb_strpos($source, 'hasMany');
                $rel_type = 'hasMany';
                break;
            case mb_strpos($source, 'belongsTo');
                $rel_type = 'belongsTo';
                break;
            default:
                $rel_type = null;
                break;
        }
        $ret['relType'] = $rel_type;

        return $ret;
    }

    static function getBelongsToMany($class_model) {
        $o       = new $class_model;
        $methods = self::getMethods($o);
        $ret     = [];
        foreach($methods as $method_name => $method) {
            $source = Arr::get($method, 'source');
            if('belongsToMany' != $method_name) {
                if(false !== mb_strpos($source, 'belongsToMany(')) {
                    $ret[$method_name] = [
                        'related'  => get_class($o->{$method_name}()->getRelated()),
                        'shotDesc' => Arr::get($method, 'shortDesc'),
                        'tags'     => Arr::get($method, 'tags'),
                        'longDesc' => Arr::get($method, 'longDesc'),
                    ];
                }
            }
        }

        return $ret;
    }

    static function getHasMany($class_model) {
        $o       = new $class_model;
        $methods = self::getMethods($o);
        $ret     = [];
        foreach($methods as $method_name => $method) {
            $source = Arr::get($method, 'source');
            if('hasMany' != $method_name) {
                if(false !== mb_strpos($source, 'hasMany(') || false !== mb_strpos($source, 'hasManyThrough(')) {
                    $ret[$method_name] = get_class($o->{$method_name}()
                        ->getRelated());
                }
            }
        }

        return $ret;
    }
}